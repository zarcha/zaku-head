#include <Servo.h>

Servo headServo; 

int servoPin = 9; //Servo pin
int min = 60; //Max positiont the servo will move to during animation
int mid = 90; //Middle position to end on
int max = 120; //Max positiont the servo will move to during animation
int pos = 0; //Current servo position
int delayMove = 30; //Time it delays before moving to the next position

int led = 10; //LED pin
int brightness = 0; //LED brightness
int fadeAmount = 5; //Amount of brightness it will change by each loop
int fadeDelay = 60; //Time between light changes in the loop

long randomNum = 0;

void setup() {
  pinMode(led, OUTPUT);
  
  RunAnimation();
}

void loop() {

  //This will create a number between 1-60 and then multiplies to by 60,000 (aka 1min) so it picks a time within an hours time till next play of the animation.
  randomNum = random(1, 60) * 60000;

  delay(randomNum);

  RunAnimation();
  
}

void RunAnimation(){
  
  OnOffLed();

  //Attaching to servo pin
  headServo.attach(servoPin);
  
  backAndForth();
  center();

  //We detach so not to create servo noise while sitting around.
  headServo.detach();

  delay(2000);

  OnOffLed();
}


//This will move it side to side
void backAndForth(){
  for (pos = mid; pos <= max; pos += 1) { 
    // in steps of 1 degree
    headServo.write(pos);             
    delay(delayMove);                      
  }
  for (pos = max; pos >= min; pos -= 1) { 
    headServo.write(pos);              
    delay(delayMove);                      
  }
}

//Centers servo.
void center(){
  while (pos != mid){
    if(pos > mid){
      pos -= 1;
    }else if(pos < mid){
      pos += 1;
    }

    headServo.write(pos);              
    delay(delayMove);
  }
  headServo.writeMicroseconds(1500);
}

//This will turn on and off the light with a fade. I had two functions but i wanted less duplication.
void OnOffLed(){

  int targetBrightness;

  if(brightness == 0){
    targetBrightness = 255;
  } else {
    targetBrightness = 0;
  }
  
  while(brightness != targetBrightness){
    Serial.println(brightness);
    
    analogWrite(led, brightness);

    if(targetBrightness == 255){
      brightness += fadeAmount;
    } else {
      brightness -= fadeAmount;
    }
    
  
    delay(fadeDelay);
  }

  if(targetBrightness == 255){
    digitalWrite(led, HIGH);
  } else {
    digitalWrite(led, LOW);
  }
}

